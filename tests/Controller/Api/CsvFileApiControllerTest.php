<?php

namespace App\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class CsvFileApiControllerTest
 *
 * @package App\Tests\Controller\Api
 */
class CsvFileApiControllerTest extends WebTestCase
{
    /**
     * @var \GuzzleHttp\Client
     */
    public static $client;
    public static $uri;

    public function setUp(): void
    {
        self::$client = new \GuzzleHttp\Client([]);
        self::$uri = $this->getKernel()->getContainer()->getParameter('domain');
    }

    public function testPost()
    {
        $options = [
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => fopen($this->getCsvFile(), 'r'),
                ],
            ],
        ];
        $response = self::$client->post(self::$uri . '/api/v1/csv/new', $options);
        $this->assertEquals(201, $response->getStatusCode());
        $data = json_decode($response->getBody());

        $this->_assertCsvFileAndInvoiceFields($data);

        return $data->file->id;
    }

    /**
     * @depends testPost
     *
     * @param int $csvFileId
     */
    public function testGet(int $csvFileId)
    {
        $response = self::$client->get(self::$uri . "/api/v1/csv/{$csvFileId}/view", []);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody());

        $this->_assertCsvFileAndInvoiceFields($data);
    }

    public function testGetAll()
    {
        $response = self::$client->get(self::$uri . '/api/v1/csv/list', []);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody());

        foreach ($data as $csv) {
            $this->assertObjectHasAttribute('id', $csv);
            $this->assertObjectHasAttribute('csv_name', $csv);
        }
    }

    /**
     * @param $data
     */
    private function _assertCsvFileAndInvoiceFields($data)
    {
        // assert file fields
        $this->assertObjectHasAttribute('file', $data);
        $this->assertObjectHasAttribute('id', $data->file);
        $this->assertObjectHasAttribute('csv_name', $data->file);

        // assert invoices data
        $this->assertObjectHasAttribute('invoices', $data);
        $this->assertCount(7, $data->invoices);
        foreach ($data->invoices as $invoice) {
            $this->assertObjectHasAttribute('id', $invoice);
            $this->assertObjectHasAttribute('invoice_id', $invoice);
            $this->assertObjectHasAttribute('amount', $invoice);
            $this->assertObjectHasAttribute('selling_price', $invoice);
            $this->assertObjectHasAttribute('due_date', $invoice);
        }

        // assert coefficient is 0.5 for record as due date is more than 30 days from current date
        $this->assertEquals(200, $data->invoices[0]->amount);
        $this->assertEquals(100, $data->invoices[0]->selling_price);

        // assert coefficient is 0.3 for record as due date is less than 30 days from current date
        $this->assertEquals(300, $data->invoices[1]->amount);
        $this->assertEquals(90, $data->invoices[1]->selling_price);

        // assert errors
        $this->assertObjectHasAttribute('errors', $data);
        $this->assertCount(3, $data->errors);
        $this->assertStringContainsString('Invalid data - Due date can not be future date.', $data->errors[0]);
    }

    /**
     * @return KernelInterface
     */
    private function getKernel(): KernelInterface
    {
        $options = [
            'environment' => 'test',
        ];
        $kernel = $this->createKernel($options);
        $kernel->boot();

        return $kernel;
    }

    /**
     * @return string
     */
    private function getCsvFile(): string
    {
        return $this->getKernel()->getProjectDir() . '/public/test.csv';
    }
}