<?php

namespace App\Manager;

use App\Entity\CsvFile;
use App\Entity\Invoice;
use App\Repository\CsvFileRepository;
use League\Csv\Reader;
use Symfony\Component\HttpFoundation\File\File;

class CsvFileManager extends BaseManager
{
    /**
     * @var InvoiceManager
     */
    private $invoiceManager;

    /**
     * CsvFileManager constructor.
     *
     * @param CsvFileRepository $csvFileRepository
     * @param InvoiceManager    $invoiceManager
     */
    public function __construct(CsvFileRepository $csvFileRepository, InvoiceManager $invoiceManager)
    {
        $this->repository = $csvFileRepository;
        $this->invoiceManager = $invoiceManager;
    }

    /**
     * @param File $csvFile
     *
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createCsvAndInvoices(File $csvFile): array
    {
        $csvFile = $this->create($csvFile);
        $results = $this->readCsvAndCreateInvoices($csvFile);

        // store invoices and errors for later use
        $csvFile->setInvoices($results['invoices']);
        $csvFile->setErrorReport($results['errors']);
        $this->save($csvFile);

        return [
            'file' => $csvFile,
            'invoices' => $results['invoices'],
            'errors' => $results['errors'],
        ];
    }

    /**
     * @param File $file
     *
     * @return CsvFile
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(File $file): CsvFile
    {
        $csvFile = new CsvFile();
        $csvFile->setCsvFile($file);
        $this->save($csvFile);

        return $csvFile;
    }

    /**
     * @param CsvFile $csvFile
     *
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function readCsvAndCreateInvoices(CsvFile $csvFile): array
    {
        $reader = Reader::createFromPath($csvFile->getCsvFile());

        $invoices = $errors = [];
        foreach ($reader->getRecords() as $row) {
            $invoice = $this->invoiceManager->createOrThrowException($csvFile, $row);
            if ($invoice instanceof Invoice) {
                $invoices[] = $invoice;
            } else {
                $errors[] = $invoice;
            }
        }

        return [
            'invoices' => $invoices,
            'errors' => $errors,
        ];
    }
}