<?php

namespace App\Manager;

use App\Entity\CsvFile;
use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use App\Util\Util;
use FOS\RestBundle\Exception\InvalidParameterException;

class InvoiceManager extends BaseManager
{
    /**
     * InvoiceManager constructor.
     *
     * @param InvoiceRepository $invoiceRepository
     */
    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->repository = $invoiceRepository;
    }

    /**
     * @param CsvFile $csvFile
     * @param array   $row
     *
     * @return Invoice|string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createOrThrowException(CsvFile $csvFile, array $row)
    {
        try {
            $params = $this->validateAndPrepareFields($row);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $this->create($csvFile, $params);
    }

    /**
     * @param CsvFile $csvFile
     * @param array   $params
     *
     * @return Invoice
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(CsvFile $csvFile, array $params)
    {
        $invoice = new Invoice();
        $invoice->setCsvFile($csvFile);
        $invoice->setInvoiceId($params['invoiceId']);
        $invoice->setAmount($params['amount']);
        $invoice->setSellingPrice($params['price']);
        $invoice->setDueDate($params['dueDate']);
        $this->save($invoice);

        return $invoice;
    }

    /**
     * @param array $row
     *
     * @return array
     * @throws \Exception
     */
    private function validateAndPrepareFields(array $row): array
    {
        if (empty($row[0]) || empty($row[1]) || empty($row[2])) {
            throw new InvalidParameterException(
                sprintf(
                    'Invalid data - You need to pass all the fields. Provided values are: `%s`',
                    implode(', ', $row)
                )
            );
        }

        if (!is_numeric($row[1]) || $row[1] < 0) {
            throw new InvalidParameterException(
                sprintf(
                    'Invalid data - Amount must be a valid number. Provided values are: `%s`',
                    implode(', ', $row)
                )
            );
        }

        return [
            'invoiceId' => $row[0],
            'amount' => $row[1],
            'price' => $this->calculatePrice($row),
            'dueDate' => Util::isValidDateOrHalt($row[2]),
        ];
    }

    /**
     * @param array $row
     *
     * @return float
     * @throws \Exception
     */
    private function calculatePrice(array $row)
    {
        $dueDate = Util::createDateTimeObjectFromString($row[2]);
        $today = new \DateTime();

        $diff = $dueDate->diff($today);

        $days = $diff->format('%R%a');

        if (0 > $days) {
            throw new InvalidParameterException(
                sprintf(
                    'Invalid data - Due date can not be future date. Provided values are: `%s`',
                    implode(', ', $row)
                )
            );
        }

        if (30 < $diff->format('%R%a')) {
            return $row[1] * 0.5;
        } else {
            return $row[1] * 0.3;
        }
    }

}