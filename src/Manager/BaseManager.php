<?php

namespace App\Manager;

use App\Repository\BaseRepository;

/**
 * Class BaseManager.
 */
abstract class BaseManager
{
    /**
     * @var mixed|BaseRepository
     */
    protected $repository;

    /**
     * @param mixed $id
     *
     * @return mixed
     */
    public function getFind($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @return mixed
     */
    public function getFindAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param mixed $parameters
     * @param mixed $orderBy
     *
     * @return array
     */
    public function getFindBy($parameters, $orderBy = null)
    {
        return $this->repository->findBy($parameters, $orderBy);
    }

    /**
     * @param mixed $parameters
     *
     * @return object|null
     */
    public function getFindOneBy($parameters)
    {
        return $this->repository->findOneBy($parameters);
    }

    /**
     * @param $object
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($object)
    {
        $this->repository->save($object);
    }

    /**
     * @param $object
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove($object)
    {
        $this->repository->remove($object);
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->repository->getEntityManager();
    }
}
