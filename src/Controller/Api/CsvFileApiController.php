<?php

namespace App\Controller\Api;

use App\Manager\CsvFileManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Exception\InvalidParameterException;
use FOS\RestBundle\Request\ParamFetcherInterface as ParamFetcher;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CsvFileApiController
 *
 * @FOSRest\Prefix("/api/v1/csv")
 * @FOSRest\NamePrefix("api_v1_csv_")
 * @package App\Controller\Api
 */
class CsvFileApiController extends AbstractFOSRestController
{
    /**
     * @var CsvFileManager
     */
    private $csvFileManager;

    /**
     * CsvFileApiController constructor.
     *
     * @param CsvFileManager $csvFileManager
     */
    public function __construct(CsvFileManager $csvFileManager)
    {
        $this->csvFileManager = $csvFileManager;
    }

    /**
     * Get List of uploaded csv files
     *
     * ## Response ##
     * [
     *      {
     *          "id": 1,
     *          "csv_name": "5ec09d0487d8a_file.csv"
     *      },
     *      ...
     * ]
     *
     * @FOSRest\Route("/list")
     * @FOSRest\View()
     */
    public function getAllAction()
    {
        return $this->csvFileManager->getFindAll();
    }

    /**
     * Get csv file details, Invoices and Error report
     *
     * ## Response ##
     * {
     *      "id": 1,
     *      "csv_name": "5ec0adc816fa2_file.csv",
     *      "invoices": [{
     *          "id": 1,
     *          "invoice_id": "123",
     *          "amount": 200,
     *          "selling_price": 100,
     *          "due_date": "2019-05-10"
     *      },
     *      ...
     *      ],
     *      "error_report": [
     *          "Invalid data - Due date can not be future date. Provided values are: `1, 100, 2020-05-20`",
     *          ...
     *      ],
     * }
     *
     * @FOSRest\Route("/{id}/view")
     * @FOSRest\View()
     * @param int $id
     *
     * @return array
     */
    public function getAction(int $id)
    {
        $csvFile = $this->csvFileManager->getFind($id);

        if (!$csvFile) {
            throw new InvalidParameterException(sprintf('Invalid ID - record for Csv ID `%s` not found', $id));
        }

        return [
            'file' => $csvFile,
            'invoices' => $csvFile->getInvoices()->getValues(),
            'errors' => $csvFile->getErrorReport(),
        ];
    }

    /**
     * Upload a csv file
     *
     * ## Response ##
     * {
     *      "id": 1,
     *      "csv_name": "5ec0adc816fa2_file.csv",
     *      "invoices": [{
     *          "id": 1,
     *          "invoice_id": "123",
     *          "amount": 200,
     *          "selling_price": 100,
     *          "due_date": "2019-05-10"
     *      },
     *      ...
     *      ],
     *      "error_report": [
     *          "Invalid data - Due date can not be future date. Provided values are: `1, 100, 2020-05-20`",
     *          ...
     *      ],
     * }
     *
     * @FOSRest\FileParam(name="file", description="CSV file", strict=true, nullable=false)
     * @FOSRest\Route("/new")
     * @FOSRest\View(statusCode=201)
     * @param ParamFetcher $paramFetcher
     *
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postCsvAction(ParamFetcher $paramFetcher)
    {
        $params = $paramFetcher->all();

        if (!$params['file'] instanceof UploadedFile) {
            throw new InvalidParameterException('Invalid `file` - given file is not valid.');
        }

        return $this->csvFileManager->createCsvAndInvoices($params['file']);
    }
}