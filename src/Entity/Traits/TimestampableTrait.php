<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Trait TimestampableTrait.
 */
trait TimestampableTrait
{
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $insertedDatetime;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedDatetime;

    /**
     * Set insertedDatetime.
     *
     * @param \DateTime $insertedDatetime
     *
     * @return self
     */
    public function setInsertedDatetime($insertedDatetime)
    {
        $this->insertedDatetime = $insertedDatetime;

        return $this;
    }

    /**
     * Get insertedDatetime.
     *
     * @return \DateTime
     */
    public function getInsertedDatetime()
    {
        return $this->insertedDatetime;
    }

    /**
     * Set updatedDatetime.
     *
     * @param \DateTime $updatedDatetime
     *
     * @return self
     */
    public function setUpdatedDatetime($updatedDatetime)
    {
        $this->updatedDatetime = $updatedDatetime;

        return $this;
    }

    /**
     * Get updatedDatetime.
     *
     * @return \DateTime
     */
    public function getUpdatedDatetime()
    {
        return $this->updatedDatetime;
    }
}
