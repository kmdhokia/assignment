<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class CsvFile
 *
 * @package App\Entity
 * @Serializer\ExclusionPolicy("all")
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="App\Repository\CsvFileRepository")
 * @ORM\Table(name="csv_files")
 */
class CsvFile
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Serializer\Expose
     */
    protected $csvName;

    /**
     * @var File
     * @Vich\UploadableField(mapping="csv", fileNameProperty="csvName")
     */
    protected $csvFile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invoice", mappedBy="csvFile", cascade={"persist","remove"})
     */
    protected $invoices;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $errorReport;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCsvName(): string
    {
        return $this->csvName;
    }

    /**
     * @param string $csvName
     */
    public function setCsvName(string $csvName): void
    {
        $this->csvName = $csvName;
    }

    /**
     * @return File
     */
    public function getCsvFile(): File
    {
        return $this->csvFile;
    }

    /**
     * @param File $csvFile
     *
     * @throws \Exception
     */
    public function setCsvFile(File $csvFile): void
    {
        $this->csvFile = $csvFile;;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoices(): \Doctrine\Common\Collections\Collection
    {
        return $this->invoices;
    }

    /**
     * @param array $invoices
     */
    public function setInvoices(array $invoices): void
    {
        $this->invoices = $invoices;
    }

    /**
     * @return array
     */
    public function getErrorReport(): array
    {
        return $this->errorReport;
    }

    /**
     * @param array $errorReport
     */
    public function setErrorReport(array $errorReport): void
    {
        $this->errorReport = $errorReport;
    }
}