<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use Doctrine\DBAL\Types\DateType;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Invoice
 *
 * @package App\Entity
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceRepository")
 * @ORM\Table(name="invoices")
 */
class Invoice
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose
     */
    protected $id;

    /**
     * @var CsvFile
     * @ORM\ManyToOne(targetEntity="App\Entity\CsvFile", inversedBy="invoices")
     * @ORM\JoinColumn(name="csv_file_id", referencedColumnName="id", nullable=false)
     */
    protected $csvFile;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     * @Serializer\Expose
     */
    protected $invoiceId;

    /**
     * @var float
     * @ORM\Column(type="float", columnDefinition="float", nullable=false)
     * @Serializer\Expose
     */
    protected $amount;

    /**
     * @var float
     * @ORM\Column(type="float", columnDefinition="float", nullable=false)
     * @Serializer\Expose
     */
    protected $sellingPrice;

    /**
     * @var DateType
     * @ORM\Column(type="date", nullable=false)
     */
    protected $dueDate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return CsvFile
     */
    public function getCsvFile(): CsvFile
    {
        return $this->csvFile;
    }

    /**
     * @param CsvFile $csvFile
     */
    public function setCsvFile(CsvFile $csvFile): void
    {
        $this->csvFile = $csvFile;
    }

    /**
     * @return string
     */
    public function getInvoiceId(): string
    {
        return $this->invoiceId;
    }

    /**
     * @param string $invoiceId
     */
    public function setInvoiceId(string $invoiceId): void
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getSellingPrice(): float
    {
        return $this->sellingPrice;
    }

    /**
     * @param float $sellingPrice
     */
    public function setSellingPrice(float $sellingPrice): void
    {
        $this->sellingPrice = $sellingPrice;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate(): \DateTime
    {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     */
    public function setDueDate(\DateTime $dueDate): void
    {
        $this->dueDate = $dueDate;
    }

}