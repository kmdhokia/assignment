<?php

namespace App\Repository;

use App\Entity\Invoice;
use Doctrine\Common\Persistence\ManagerRegistry;

class InvoiceRepository extends BaseRepository
{
    /**
     * CsvFileRepository constructor.
     *
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Invoice::class);
    }
}