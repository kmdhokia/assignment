<?php

namespace App\Repository;

use App\Entity\CsvFile;
use Doctrine\Common\Persistence\ManagerRegistry;

class CsvFileRepository extends BaseRepository
{
    /**
     * CsvFileRepository constructor.
     *
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, CsvFile::class);
    }
}