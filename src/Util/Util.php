<?php

namespace App\Util;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Util
{
    const DATE_FORMAT = 'Y-m-d';

    /**
     * @param string $date
     *
     * @return bool
     */
    public static function isValidDate(string $date)
    {
        $d = \DateTime::createFromFormat(self::DATE_FORMAT, $date);

        return $d && $d->format(self::DATE_FORMAT) == $date;
    }

    /**
     * @param string|null $dateString
     *
     * @return \DateTime|null
     */
    public static function createDateTimeObjectFromString(?string $dateString): ?\DateTime
    {
        return \DateTime::createFromFormat(self::DATE_FORMAT, $dateString) ?: null;
    }

    /**
     * @param string $date
     * @param string $dateFieldname
     *
     * @return \DateTime|null
     */
    public static function isValidDateOrHalt(string $date, $dateFieldname = 'due_date')
    {
        $isValid = self::isValidDate($date);

        if (!$isValid) {
            throw new BadRequestHttpException(sprintf('Invalid `%s` - The date passed of `%s` do not matches the needed `%s` format.', $dateFieldname, $date, self::DATE_FORMAT), Response::HTTP_BAD_REQUEST);
        }

        return self::createDateTimeObjectFromString($date);
    }
}
