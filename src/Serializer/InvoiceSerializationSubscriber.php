<?php

namespace App\Serializer;

use App\Entity\Invoice;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;

class InvoiceSerializationSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event' => 'serializer.post_serialize',
                'method' => 'onPostSerialize',
                'format' => 'json',
                'class' => Invoice::class,
            ],
        ];
    }

    /**
     * @param ObjectEvent $event
     *
     * @throws \Exception
     */
    public function onPostSerialize(ObjectEvent $event)
    {
        $invoice = $event->getObject();

        if (!$invoice instanceof Invoice) {
            return;
        }

        $visitor = $event->getVisitor();

        $visitor->setData('due_date', $invoice->getDueDate()->format('Y-m-d'));
    }
}