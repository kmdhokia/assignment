Please follow below given steps to test the application / phpunit successfully.

1. Set `domain` parameter in services.yaml file
2. Go to project directory in console and execute `composer install` command.
3. To execute tests, execute `php bin/phpunit` command
4. To test the application in browser, just use the domain name you set in services.yaml file. 
